import {Skill} from "./skill.models";

export interface Project {

  _id: string,
  name: string,
  subtitle: string,
  description: string,
  shortDescription: string,
  img: string,
  url: string,
  demoUrl: string,
  skills: Skill[],
  type: string,
  status: boolean,
  bgImg: string,
  screenshots?: string[],

}
