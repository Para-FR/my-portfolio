export class GlobalConstants {

  // URLs
  public static authApiURL: string = '';
  public static apiURLP: string = '';
  public static apiURLU: string = '';
  public static apiURLD: string = '';
  public static apiURLL: string = '';
  public static apiURLA: string = '';
  public static apiURLAssets: string = '';
  public static apiURL: string = '';
  public static apiURLBase: string = '';
  public static socketIOUrl: string = '';
  public static mode: string = 'prod'

  // Initialise les constantes en fonction de l'environnement
  public static initConstants() {
    if (this.mode === 'local') {
      this.authApiURL = 'http://localhost:9770/auth/';
      this.apiURLP = 'http://localhost:9770/p/';
      this.apiURLU = 'http://localhost:9770/u/';
      this.apiURLD = 'http://localhost:9770/data/';
      this.apiURLL = 'http://localhost:9770/lobby';
      this.apiURLA = 'http://localhost:9770/a/';
      this.apiURLAssets = 'http://localhost:9770/public/';
      this.apiURL = 'http://localhost:9770/api/';
      this.apiURLBase = 'http://localhost:9770';
    } else {
      this.authApiURL = 'https://parapi.pull-push.fr/auth/';
      this.apiURLP = 'https://parapi.pull-push.fr/p/';
      this.apiURLU = 'https://parapi.pull-push.fr/u/';
      this.apiURLD = 'https://parapi.pull-push.fr/data/';
      this.apiURLL = 'https://parapi.pull-push.fr/lobby';
      this.apiURLA = 'https://parapi.pull-push.fr/a/';
      this.apiURLAssets = 'https://parapi.pull-push.fr/public/';
      this.apiURL = 'https://parapi.pull-push.fr/api/';
      this.apiURLBase = 'https://parapi.pull-push.fr';
    }
  }

  public static siteTitle = 'RRQ';
}

GlobalConstants.initConstants();
