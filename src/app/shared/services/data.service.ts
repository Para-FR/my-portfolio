import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {GlobalConstants} from "../common/global-constant";
import {Observable} from "rxjs";
import {Project} from "../models/project.models";


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  retrieveProjects(): Observable<Project[]> {
    return this.http.get<Project[]>(`${GlobalConstants.apiURLD}projects`)
  }

  retrievePersonalProjects(): Observable<Project[]> {
    return this.http.get<Project[]>(`${GlobalConstants.apiURLD}projects/personal`)
  }

  retrieveSingleProject(url: string): Observable<Project> {
    return this.http.get<Project>(`${GlobalConstants.apiURLD}projects/${url}`)
  }

  retrieveApiStatus(): Observable<any> {
    return this.http.get(`${GlobalConstants.apiURLBase}`)
  }
}
