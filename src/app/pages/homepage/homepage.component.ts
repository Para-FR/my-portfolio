import { Component } from '@angular/core';
import {DataService} from "../../shared/services/data.service";
import {Project} from "../../shared/models/project.models";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent {

  public projects: Project[] = []
  public personalProjects: Project[] = []

  constructor(private dataService: DataService) { }


  ngOnInit(): void {
    this.retrieveProjects()
    this.retrievePersonalProjects()
  }

  private retrieveProjects() {

    this.dataService.retrieveProjects()
      .subscribe(projects => {
        console.log('projects ➡️', projects)
        this.projects = projects
      })

  }

  private retrievePersonalProjects() {

      this.dataService.retrievePersonalProjects()
        .subscribe(projects => {
          console.log('projects ➡️', projects)
          this.personalProjects = projects
        })
  }
}
