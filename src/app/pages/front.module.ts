import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FrontRoutingModule } from './front-routing.module';
import { HomepageComponent } from './homepage/homepage.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';

@NgModule({
  declarations: [
    HomepageComponent,
    ProjectDetailsComponent
  ],
  exports: [
  ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FrontRoutingModule,
    ]
})
export class FrontModule { }
