import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {HomepageComponent} from "./homepage/homepage.component";
import {ProjectDetailsComponent} from "./project-details/project-details.component";


const routes: Routes = [
  {
    path: 'homepage',
    redirectTo: ''
  },
  {
    path: '#projects',
    component: HomepageComponent
  },
  {
    path: '',
    component: HomepageComponent
  },
  {
    path: 'project/:url',
    component: ProjectDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule, FormsModule],
  declarations: [],
  exports: [RouterModule]
})
export class FrontRoutingModule {
}
