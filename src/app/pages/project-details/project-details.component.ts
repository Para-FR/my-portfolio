import { Component } from '@angular/core';
import {DataService} from "../../shared/services/data.service";
import {ActivatedRoute} from "@angular/router";
import {Project} from "../../shared/models/project.models";
import {GlobalConstants} from "../../shared/common/global-constant";

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent {

  public currentProjectUrl: any = this.route.snapshot.paramMap.get('url')
  public currentProject: Project = {
    _id: "",
    demoUrl: "",
    description: "",
    img: "",
    name: "",
    subtitle: "",
    shortDescription: "",
    skills: [],
    status: false,
    type: "",
    url: "",
    bgImg: '',
    screenshots: []
  }
  public assetsUrl: string = GlobalConstants.apiURLAssets
  public currentScreenshotUrl: string = ''
  public displayModal: boolean = false

  constructor(private dataService: DataService, private route: ActivatedRoute) {


  }

  ngOnInit() {

    console.log('this.currentProjectUrl ➡️', this.currentProjectUrl)

    this.dataService.retrieveSingleProject(this.currentProjectUrl)
      .subscribe((data) => {
        console.log(data)
        this.currentProject = data
      })
  }

  openInPreview(url: string, screenshot: string) {
    this.displayModal = true

    this.currentScreenshotUrl = this.assetsUrl + 'projects/screenshots/' + url + '/' + screenshot
  }

  closePreview() {
    this.displayModal = false
    this.currentScreenshotUrl = ''
  }
}
