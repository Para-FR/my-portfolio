import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: 'homepage',
    redirectTo: ''
  },
  {
    path: '',
    loadChildren: () => import('./pages/front.module').then(m => m.FrontModule)
  },
  {
    'path': '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
