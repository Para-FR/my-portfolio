import {Component} from '@angular/core';
import {DataService} from "../../shared/services/data.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  discord: string = 'Para#1860';
  currentApiStatus: string = ''

  constructor(private dataService: DataService) {

    this.getApiStatus()
  }

  getApiStatus() {
    this.dataService.retrieveApiStatus()
      .subscribe(
        (response: any) => {
          if (response) {

            console.log(response)
            console.log('online')

            this.currentApiStatus = 'online'


          }

        }, (error: any) => {
          this.currentApiStatus = 'offline'
          console.log('offline')
        }
      )
  }
}
