import { Component } from '@angular/core';
import {ViewportScroller} from "@angular/common";
import {NavigationEnd, Router} from "@angular/router";
import {filter} from "rxjs";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  public projectAnchor: boolean = false;

  constructor(private viewportScroller: ViewportScroller, private router: Router) {

    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: any) => {
        const currentUrl = event.url;

        console.log('currentUrl ➡️', currentUrl.length)

        if (currentUrl !== '/') {
          this.projectAnchor = false
        } else if (currentUrl == '/#projects'){
          console.log('coucou')
          this.scrollToSection('projects')
          this.projectAnchor = true
        } else {
          this.projectAnchor = true
        }

        console.log('this.projectAnchor ➡️', this.projectAnchor)
      });

  }

  scrollToSection(sectionId: string) {
    const yOffset = 94.5; // Specify a negative offset to scroll 130px upward

    this.viewportScroller.setOffset([0, yOffset]); // Apply the offset to the viewport scroller

    console.log('sectionId ➡️', sectionId)
    // Check if the element exists before scrolling
    if (document.getElementById(sectionId)) {
      // Scroll to the specified section using the set offset
      this.viewportScroller.scrollToAnchor(sectionId);

    } else {
      console.warn(`Element with id "${sectionId}" not found.`);
    }
  }

}
